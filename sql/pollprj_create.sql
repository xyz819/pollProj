﻿

	drop table P_RGRelation;
	drop table P_USER;
	drop sequence P_USER_id;
	drop table P_GROUP;
	drop sequence P_GROUP_id;
	drop table P_Right;
	drop sequence P_Right_id;

	-- Sequence: P_GROUP_ID
	CREATE SEQUENCE P_GROUP_ID
	      START WITH 100;

	-- Sequence: P_Right_id
	CREATE SEQUENCE P_Right_id
	      START WITH 100;

	-- Sequence: P_USER_ID
	CREATE SEQUENCE P_USER_ID
	      START WITH 100;

    --权限组
	CREATE TABLE P_GROUP (
		groupid number(7,0)  NOT NULL,
		--权限组名
		groupname varchar2(50)  NOT NULL,
		CONSTRAINT P_GROUP_pk PRIMARY KEY (GROUPID)
	) ;
	INSERT INTO P_GROUP VALUES (1,'管理员');
	INSERT INTO P_GROUP VALUES (2,'学生');
	INSERT INTO P_GROUP VALUES (3,'教师');

	-- Table: P_USER
	--用户表
	CREATE TABLE P_USER (
		userid number(7,0)  NOT NULL,
		--外键
		groupid number(7,0)  NOT NULL,
		--用户名
		username varchar2(50)  NOT NULL,
		--密码
		password varchar2(20)  NOT NULL,
		--电话号码
		phone_num varchar2(11)  NOT NULL,
		--邮箱
		email varchar2(50)  NOT NULL,
		--注册时间
		init_time date  NOT NULL,
		--最后登录时间
		last_login_time date  NOT NULL,
		--登录次数
		login_count number(5,0)  NOT NULL,
		CONSTRAINT P_USER_pk PRIMARY KEY (userid),
		FOREIGN KEY(groupid) REFERENCES P_GROUP (groupid)
	) ;
	INSERT INTO P_USER VALUES(1,1,'admin','admin','110','110@qq.com',to_date('20000101000000','yyyymmddhh24miss'),sysdate,0);
	INSERT INTO P_USER VALUES(2,2,'李雷','admin','13269011571','110@qq.com',to_date('20130902121000','yyyymmddhh24miss'),sysdate,12);
	INSERT INTO P_USER VALUES(3,2,'韩梅梅','admin','13185912451','110@qq.com',to_date('20130901083000','yyyymmddhh24miss'),sysdate,52);
	INSERT INTO P_USER VALUES(4,2,'刘邦','admin','13671920154','110@qq.com',to_date('20130904070000','yyyymmddhh24miss'),sysdate,33);
	INSERT INTO P_USER VALUES(5,3,'诸葛亮','admin','13776241341','110@qq.com',to_date('20100820080000','yyyymmddhh24miss'),sysdate,25);
	INSERT INTO P_USER VALUES(6,3,'尼禄','admin','18821249021','110@qq.com',to_date('20020712231829','yyyymmddhh24miss'),sysdate,121);

	-- Table: P_Right
	--权限表
	CREATE TABLE P_Right (
		rightid number(7,0)  NOT NULL,
		--权限名
		rightname varchar2(50)  NOT NULL,
		--权限判定的url
		righturl  varchar(50)	NOT NULL,
		--父权限
		parent_right number(7,0)  NOT NULL,
		--备注
		description varchar2(200)  NOT NULL,
		CONSTRAINT P_Right_pk PRIMARY KEY (rightid)
	) ;
	  INSERT INTO P_Right VALUES (99,'All','base',99,'无');
		INSERT INTO P_Right VALUES (1,'考评管理','pages/',99,'菜单标题');
		INSERT INTO P_Right VALUES (2,'未审查询','unChecked/view/unChecked.html',1,'无');
		INSERT INTO P_Right VALUES (3,'已审查询','checked/view/checked.html',1,'无');
		INSERT INTO P_Right VALUES (4,'考评列表','evalList/view/evalList.html',1,'无');
		INSERT INTO P_Right VALUES (5,'考评模板','evalTempl/view/evalTempl.html',1,'无');
		INSERT INTO P_Right VALUES (6,'班级管理','pages/',99,'菜单标题');
		INSERT INTO P_Right VALUES (7,'新增班级','classNew/view/classNew.html',6,'无');
		INSERT INTO P_Right VALUES (8,'修改班级','classMod/view/classMod.html',6,'无');
		INSERT INTO P_Right VALUES (9,'统计分析','pages/',99,'无');
		INSERT INTO P_Right VALUES (10,'按月统计','statisticbym/view/statisticbym.html',9,'无');
		INSERT INTO P_Right VALUES (11,'按季统计','statisticbys/view/statisticbys.html',9,'无');
		INSERT INTO P_Right VALUES (12,'按年统计','statisticbyy/view/statisticbyy.html',9,'无');
		INSERT INTO P_Right VALUES (13,'用户管理','pages/',99,'菜单标题');
		INSERT INTO P_Right VALUES (14,'用户列表','pages/users/view/users.html',13,'无');
		INSERT INTO P_Right VALUES (15,'角色权限','pages/userlimits/view/userlimits.html',13,'无');
		INSERT INTO P_Right VALUES (16,'个人中心','pages/',99,'无');
		INSERT INTO P_Right VALUES (17,'个人信息','personal/view/personal.html',16,'无');
		INSERT INTO P_Right VALUES (18,'修改密码','pages/pwdmod/view/pwdmod.html',16,'无');

	--Table: P_RGRelation
	--权限组和权限多对多关联
	CREATE TABLE P_RGRelation (
		--right外键
		rightid number(7,0)  NOT NULL,
		--group外键
		groupid number(7,0)  NOT NULL,
		CONSTRAINT P_RGRelation_pk PRIMARY KEY (rightid,groupid),	
		FOREIGN KEY (groupid) REFERENCES P_GROUP (groupid),
		FOREIGN KEY (rightid) REFERENCES P_Right (rightid)
	) ;
	INSERT INTO P_RGRelation VALUES(1,1);
		INSERT INTO P_RGRelation VALUES(2,1);
			INSERT INTO P_RGRelation VALUES(3,1);
				INSERT INTO P_RGRelation VALUES(4,1);
					INSERT INTO P_RGRelation VALUES(5,1);
						INSERT INTO P_RGRelation VALUES(6,1);
							INSERT INTO P_RGRelation VALUES(7,1);
								INSERT INTO P_RGRelation VALUES(8,1);
									INSERT INTO P_RGRelation VALUES(9,1);
										INSERT INTO P_RGRelation VALUES(10,1);
											INSERT INTO P_RGRelation VALUES(11,1);
												INSERT INTO P_RGRelation VALUES(12,1);
													INSERT INTO P_RGRelation VALUES(13,1);
														INSERT INTO P_RGRelation VALUES(14,1);
															INSERT INTO P_RGRelation VALUES(15,1);
																INSERT INTO P_RGRelation VALUES(16,1);
																	INSERT INTO P_RGRelation VALUES(17,1);
																		INSERT INTO P_RGRelation VALUES(18,1);
  	INSERT INTO P_RGRelation VALUES(1,2);
      INSERT INTO P_RGRelation VALUES(4,2);
        INSERT INTO P_RGRelation VALUES(9,2);
          INSERT INTO P_RGRelation VALUES(10,2);
            INSERT INTO P_RGRelation VALUES(11,2);
              INSERT INTO P_RGRelation VALUES(12,2);
                INSERT INTO P_RGRelation VALUES(16,2);
                  INSERT INTO P_RGRelation VALUES(17,2);
                    INSERT INTO P_RGRelation VALUES(18,2);
  	INSERT INTO P_RGRelation VALUES(1,3);
      INSERT INTO P_RGRelation VALUES(4,3);
        INSERT INTO P_RGRelation VALUES(6,3);
          INSERT INTO P_RGRelation VALUES(7,3);
            INSERT INTO P_RGRelation VALUES(8,3);
              INSERT INTO P_RGRelation VALUES(9,3);
                INSERT INTO P_RGRelation VALUES(10,3);
                  INSERT INTO P_RGRelation VALUES(11,3);
                    INSERT INTO P_RGRelation VALUES(12,3);
                      INSERT INTO P_RGRelation VALUES(16,3);
                        INSERT INTO P_RGRelation VALUES(17,3);
                          INSERT INTO P_RGRelation VALUES(18,3);


	drop table P_Detail;
	drop sequence P_Detail_id;
	drop table P_personal;
	drop sequence P_personal_id;
	drop table P_Result;
	drop sequence P_Result_id;

	-- Sequence: p_detail_id
	CREATE SEQUENCE p_detail_id
	      START WITH 100;

	-- Sequence: P_Result_ID
	CREATE SEQUENCE P_Result_ID
	      START WITH 100;

	-- Sequence: P_personal_id
	CREATE SEQUENCE P_personal_id
	      START WITH 100;

	-- Table: P_Result
	--审核结果表
	CREATE TABLE P_Result (
		Resultid number(7,0)  NOT NULL,
		--结果编号
		Result_sid varchar2(20)  NOT NULL,
		--班级名
		Clazzname varchar2(50)  NOT NULL,
		--班级类型
		ClazzType varchar2(50)  NOT NULL,
		--被调查人名字
		victimName varchar2(50)  NOT NULL,
		--开始调查时间
		StartTime date  NOT NULL,
	   --结束调查时间
		EndTime date  NOT NULL,
		--参与调查人数量
		Usercount number(6,0)  NOT NULL,
		--有效人数
		validity number(6,0)  NOT NULL,
		--结果状态（已审核（1），未审核（0））
		result_state	number(2,0)  NOT NULL,
		--平均星值（最高五星）
		staravg varchar2(10)  NOT NULL,
		CONSTRAINT P_Result_pk PRIMARY KEY (Resultid)
	) ;
	INSERT INTO P_Result VALUES(1,'a8102001','土木工程三班','土木工程','韩静',to_date('20180101000000','yyyymmddhh24miss'),to_date('20180102000000','yyyymmddhh24miss'),3000,2900,1,'5');
	INSERT INTO P_Result VALUES(2,'a8102002','信息技术一班','信息技术','李和谐',to_date('20171226081203','yyyymmddhh24miss'),to_date('20171228182959','yyyymmddhh24miss'),1321,1029,1,'4');
	INSERT INTO P_Result VALUES(3,'a8102003','电焊一班','焊接技术','赵杰',to_date('20161211080001','yyyymmddhh24miss'),to_date('20161230185959','yyyymmddhh24miss'),2190,1981,1,'3');

	--Table: P_Personal
	--单次提交结果表
	CREATE TABLE P_Personal(
		personalid number(7,0)  NOT NULL,
		--Result外键
		Resultid number(7,0)  NOT NULL,
		--平均星值（最高五星）
		staravg varchar2(10)  NOT NULL,
		--是否有效（有效（1），无效（0））
		personal_state	 number(2,0)  NOT NULL,	
		--提交时间
	    sendTime date  NOT NULL,
	    CONSTRAINT P_Personal_pk PRIMARY KEY (personalid),
		FOREIGN KEY (Resultid) REFERENCES P_Result (Resultid)	
   	) ;
	INSERT INTO P_Personal VALUES(1,1,'5',1,to_date('20180101122819','yyyymmddhh24miss'));
	INSERT INTO P_Personal VALUES(2,1,'5',1,to_date('20180101080001','yyyymmddhh24miss'));
	INSERT INTO P_Personal VALUES(3,1,'5',1,to_date('20180101211920','yyyymmddhh24miss'));
	INSERT INTO P_Personal VALUES(4,1,'5',0,to_date('20180101191201','yyyymmddhh24miss'));

	-- Table: P_Detail
	--详细描述
	CREATE TABLE P_Detail (
	    Detailid number(7,0)  NOT NULL,
		--personal外键
	    personalid number(7,0)  NOT NULL,
	    --明细Key(问题)
	    q_key varchar2(400)  NOT NULL,
		--明细value(答案)
	    a_value varchar2(1000)  NOT NULL,
		--评分(1),建议(0)
	    key_type number(2,0) NOT NULL,
	    CONSTRAINT P_Detail_pk PRIMARY KEY (Detailid),
		FOREIGN KEY (personalid) REFERENCES P_Personal (personalid)	
	) ;
	INSERT INTO P_Detail VALUES(1,1,'对老师的建议','老师真棒',0);
	INSERT INTO P_Detail VALUES(2,1,'教育质量::老师上课生动风趣','5',1);
	INSERT INTO P_Detail VALUES(3,1,'教育质量::老师及时布置作业','5',1);
	INSERT INTO P_Detail VALUES(4,1,'教育质量::老师按时批阅作业','5',1);
	INSERT INTO P_Detail VALUES(5,1,'自身形象::着装整齐','5',1);
	INSERT INTO P_Detail VALUES(6,1,'自身形象::普通话标准','5',1);
	INSERT INTO P_Detail VALUES(7,1,'对老师的建议','老师非常不错,希望继续加油',0);
	INSERT INTO P_Detail VALUES(8,1,'教育质量::老师上课生动风趣','4',1);
	INSERT INTO P_Detail VALUES(9,1,'教育质量::老师及时布置作业','3',1);
	INSERT INTO P_Detail VALUES(10,1,'教育质量::老师按时批阅作业','4',1);
	INSERT INTO P_Detail VALUES(11,1,'自身形象::着装整齐','4',1);
	INSERT INTO P_Detail VALUES(12,1,'自身形象::普通话标准','5',1);
	INSERT INTO P_Detail VALUES(13,1,'对老师的建议','老师真棒',0);
	INSERT INTO P_Detail VALUES(14,1,'教育质量::老师上课生动风趣','5',1);
	INSERT INTO P_Detail VALUES(15,1,'教育质量::老师及时布置作业','4',1);
	INSERT INTO P_Detail VALUES(16,1,'教育质量::老师按时批阅作业','5',1);
	INSERT INTO P_Detail VALUES(17,1,'自身形象::着装整齐','4',1);
	INSERT INTO P_Detail VALUES(18,1,'自身形象::普通话标准','5',1);



	drop table P_CLAZZ;
	drop sequence P_CLAZZ_id;
	drop table P_CLAZZTYPE;
	drop sequence P_CLAZZTYPE_id;

	CREATE SEQUENCE P_CLAZZTYPE_ID
	      START WITH 100;

	-- Sequence: P_CLAZZ_ID
	CREATE SEQUENCE P_CLAZZ_ID
	      START WITH 100;

	CREATE TABLE P_CLAZZTYPE (
		--班级类型id
		clazztypeid number(7,0)  NOT NULL,
		--班级类型名
		clazztypename varchar2(50)  NOT NULL,
		--班级类型描述
		description varchar2(200)  NOT NULL,
		CONSTRAINT P_CLAZZTYPE_pk PRIMARY KEY (clazztypeid)
	) ;
	INSERT INTO P_CLAZZTYPE VALUES(1,'土木工程','无');
	INSERT INTO P_CLAZZTYPE VALUES(2,'建筑桥梁','无');
	INSERT INTO P_CLAZZTYPE VALUES(3,'信息技术','无');
	INSERT INTO P_CLAZZTYPE VALUES(4,'人机工程','无');
	INSERT INTO P_CLAZZTYPE VALUES(5,'软件工程','无');

	CREATE TABLE P_CLAZZ (
		clazzid number(7,0)  NOT NULL,
		--clazztype外键（班级类型）
		clazztypeid number(7,0)  NOT NULL,
		--班级名字
		clazzname varchar2(50)  NOT NULL,
		--班级创建时间
		clazz_create_date date  NOT NULL,
		--描述
		description varchar2(200)  NOT NULL,
		--班级状态（毕业（1）和未毕业（0））
		clazz_state number(2,0)  NOT NULL,
		CONSTRAINT P_CLAZZ_pk PRIMARY KEY (clazzid),
		
		FOREIGN KEY (clazztypeid) REFERENCES P_CLAZZTYPE (clazztypeid)
	) ;
	INSERT INTO P_CLAZZ VALUES(1,1,'土木一班',to_date('20130901122819','yyyymmddhh24miss'),'无',1);
	INSERT INTO P_CLAZZ VALUES(2,1,'土木二班',to_date('20140901122819','yyyymmddhh24miss'),'无',1);
	INSERT INTO P_CLAZZ VALUES(3,1,'土木三班',to_date('20150901122819','yyyymmddhh24miss'),'无',1);
	INSERT INTO P_CLAZZ VALUES(4,2,'建筑一班',to_date('20140901122819','yyyymmddhh24miss'),'无',1);
	INSERT INTO P_CLAZZ VALUES(5,2,'建筑二班',to_date('20150901122819','yyyymmddhh24miss'),'无',1);


	drop table P_Advice;
	drop sequence P_Advice_id;
	drop table P_Question;
	drop sequence P_Question_id;
	drop table P_QMenu;
	drop sequence P_QMenu_id;
	drop table P_Temple;
	drop sequence P_Temple_id;

	      
	-- Sequence: P_Qmenu_id
	CREATE SEQUENCE P_Qmenu_id
	      START WITH 100;

	-- Sequence: P_Question_Id
	CREATE SEQUENCE P_Question_Id
	      START WITH 100;
	      
	-- Sequence: P_Temple_id
	CREATE SEQUENCE P_Temple_id
	      START WITH 100;

	-- Sequence: P_advice_id
	CREATE SEQUENCE P_advice_id
	      START WITH 100;

	-- Table: P_Temple
	--模板
	CREATE TABLE P_Temple (
		templeid number(7,0)  NOT NULL,
		--模板名称
		templename varchar2(50)  NOT NULL,
		--模版编号
		temple_sid varchar2(20)  NOT NULL,
		--模板状态(1可用,0禁用)
		temple_state varchar2(2)  NOT NULL,
		--备注
		description varchar2(200)  NOT NULL,
		CONSTRAINT P_Temple_pk PRIMARY KEY (templeid)
	) ;
	INSERT INTO P_Temple VALUES(1,'模版一','a001',1,'无');
	INSERT INTO P_Temple VALUES(2,'模版二','a002',1,'无');
	INSERT INTO P_Temple VALUES(3,'模版三','a003',1,'无');

	--类目表
	CREATE TABLE P_QMenu (
		QMenuid number(7,0)  NOT NULL,
		--temple外键
		templeid number(7,0)  NOT NULL,
		--类目名
		Qmenu_name varchar2(50)  NOT NULL,
		CONSTRAINT P_QMenu_pk PRIMARY KEY (QMenuid),
		FOREIGN KEY(templeid) REFERENCES P_Temple (templeid)
	) ;
	INSERT INTO P_QMenu VALUES(1,1,'教育质量');
	INSERT INTO P_QMenu VALUES(2,1,'自身形象');
	INSERT INTO P_QMenu VALUES(3,2,'教育质量');
	INSERT INTO P_QMenu VALUES(4,2,'自身形象');
	INSERT INTO P_QMenu VALUES(5,2,'师德素质');

	CREATE TABLE P_Question (
		Questionid number(7,0)  NOT NULL,
		--QMenu外键
		QMenuid number(7,0)  NOT NULL,
		--问题内容
		Question_Name varchar2(50)  NOT NULL,
		CONSTRAINT P_Question_pk PRIMARY KEY (Questionid),
		FOREIGN KEY (QMenuid) REFERENCES P_QMenu (QMenuid)
	) ;
	INSERT INTO P_Question VALUES(1,1,'老师上课生动风趣');
	INSERT INTO P_Question VALUES(2,1,'老师及时布置作业');
	INSERT INTO P_Question VALUES(3,1,'老师及时批改作业');
	INSERT INTO P_Question VALUES(4,2,'着装整洁');
	INSERT INTO P_Question VALUES(5,2,'普通话标准');
	INSERT INTO P_Question VALUES(6,3,'老师上课生动风趣');
	INSERT INTO P_Question VALUES(7,3,'老师及时布置作业');
	INSERT INTO P_Question VALUES(8,3,'老师及时批改作业');
	INSERT INTO P_Question VALUES(9,4,'着装整洁');
	INSERT INTO P_Question VALUES(10,4,'普通话标准');
	INSERT INTO P_Question VALUES(11,5,'素质高');

	CREATE TABLE P_Advice (
		adviceid number(7,0)  NOT NULL,
		--temple外键
		templeid number(7,0)  NOT NULL,
		--建议名
		Advicename varchar2(50)  NOT NULL,
		CONSTRAINT P_Advice_pk PRIMARY KEY (adviceid),
		FOREIGN KEY (templeid) REFERENCES P_Temple (templeid)
	) ;
	INSERT INTO P_Advice VALUES(1,1,'对老师印象');
	INSERT INTO P_Advice VALUES(2,2,'对老师印象');
	INSERT INTO P_Advice VALUES(3,2,'想对老师说的话');