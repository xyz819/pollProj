<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html ng-app="login" ng-controller="login" xmlns:c="http://www.w3.org/1999/XSL/Transform">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Cache" content="no-cache">
    <link rel="Shortcut Icon" href="${base}/favicon.ico?v=17081300">
    <meta name="keywords" content="天智教育">
    <meta name="description" content="天智教育在线考评系统">
    <title>天智教师考评|Login</title>
    <link rel="stylesheet" href="${base}/lib/bootstrap-3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${base}/lib/layui/css/layui.min.css">
    <link rel="stylesheet" type="text/css" href="${base}/css/main.css"/>
    <link rel="stylesheet" type="text/css"
          href="${base}/js/lib/angular-plugins/angular-block-ui/angular-block-ui.min.css"/>
    <script type="text/javascript" src="${base}/lib/ng.js"></script>
</head>

<body class="layui-layout-body loginbody">
<header>
    <img class="loginbg" src="${base}/images/login_bg.png" alt=""/>
    <div class="title">
        <img class="" src="${base}/images/icons/logo_login.png" alt="天智教育"/>
        <div>
            <p class="a">在线考评系统</p>
            <p class="b">Online Evaluation System</p>
        </div>
    </div>
</header>
<section>
    <div class="fly-panel fly-panel-user">
        <div class="layui-tab layui-tab-brief" lay-filter="user">
            <!--<ul class="layui-tab-title">
            <li class="layui-this">登入</li>
            <li>
                <a href="/user/reg/">注册</a>
            </li>
        </ul>-->
            <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                <div class="layui-tab-item layui-show">
                    <div class="layui-form layui-form-pane">
                        <form class="layui-form" method="post" action="${base }/user/login">
                            <c:if test="${info == 1 }">
                                <b style='color: red'>用户名或者密码有误!</b>
                            </c:if>
                            <div class="layui-form-item"><label for="L_email" class="layui-form-label"  value="${username}">用户名</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_email" name="username" lay-verify="required"
                                           autocomplete="off" class="layui-input"></div>
                            </div>
                            <div class="layui-form-item"><label for="L_pass" class="layui-form-label"  value="${password}">密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" id="L_pass" name="password" required="" lay-verify="required"
                                           autocomplete="off" class="layui-input">
                                </div>
                            </div>


                            <!--<div class="layui-form-item"> <label for="L_vercode" class="layui-form-label">人类验证</label>
                            <div class="layui-input-inline"> <input type="text" id="L_vercode" name="vercode" required="" lay-verify="required" placeholder="请回答后面的问题" autocomplete="off" class="layui-input"> </div>

                        </div>-->
                            <div class="layui-form-item">
                                <!--<span style="padding-left:20px;"> <a href="#">忘记密码？</a> </span>-->
                                <button class="layui-btn" lay-submit lay-filter="login">立即登录</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- footer -->
<div class="layui-footer" style="color: #888;">
    ©天创集团（中国）2017 版权所有<br/> TianZhi Education Student Feedback Sysytem | 天智教育学院反馈系统 | 2009-2020
</div>
<script src="${base}/lib/layui/layui.js"></script>
<script>
    var app = angular.module("login", [])
        .controller("login", function ($scope) {
            layui.use(['element', 'form'], function () {
                var element = layui.element,
                    form = layui.form;
                form.render();
                //监听提交
                form.on('submit(login)', function (data) {
                    // layer.msg(JSON.stringify(data.field),function(){
                    // 	window.location.href = "./index.html"
                    // });
                    return true;
                });
            });

        })
</script>
</body>

</html>