define(['app', 'blockUI', 'jquery'], function(app) {
	app.controller('evalTempl', function($scope, $rootScope, $http, blockUI, $timeout, $modal, $log) {

		//		blockUI.start("数据读取中~")
		//		$timeout(function() {
		//			$scope.$apply(function() {
		//				blockUI.stop();
		//			});
		//		}, 2000, false)
		//初始化表格。获取当前所有模板
        $scope.init = function() {
            $http({
                method: "GET",
                url: "/pollProj/Templ/evalTempl?page=1"
            }).then(function (res) {
                $scope.data  = res.data;
                console.log($scope.data.totalPages);
            }, function (err) {
                alert("init err");
            })
    }

		$scope.all = false;
		$scope.titleShow = true;
		$scope.allChk = function() {
			$scope.titleShow = !$scope.all;
			$rootScope.data.data.map(function(value) {
				value.chk = $scope.all;
				console.log(value.chk)
				return value
			})
		}
		$scope.itemChk = function(val) {
			console.log(val)
			var anyonetrue = false,
				anyonefalse = false
			for(var i = 0; i < $rootScope.data.data.length; i++) {
				if($rootScope.data.data[i].chk == false) {
					$scope.all = false;
					anyonefalse = true
				} else {
					anyonetrue = true
				}
			}
			$scope.all = !anyonefalse;
			$scope.titleShow = !anyonetrue;
			if(!val) {
				$scope.all = false;
			} else {
				$scope.titleShow = false;
			}

		}

		$scope.addtempl = function() {

            var data1 ={totalPages:[{
				templeId:($scope.totalRecords+1),
				templeName:"",
                temple_sid:"",
                temple_state:1,
                description:"",
				advices:[{
					adviceId:1,
					advicename:"建议1"
				}],
				qmemus:[{
					qmenuId:1,
					qmenu_Name:"类目1",
					questions:{
						questionId:1,
						question_Name:"问题1"
					}
				}]
			}]}
 		$rootScope.data.pageDatas.unshift({
            templeId:($scope.totalRecords+1),
            templeName:"",
            temple_sid:"",
            temple_state:1,
            description:"",
            advices:[{
                adviceId:1,
                advicename:""
            }],
            qmemus:[{
                qmenuId:1,
                qmenu_Name:"",
                questions:{
                    questionId:1,
                    question_Name:""
                }
            }]
        })
		alert(JASONscope.data.pageDatas[0])
        $scope.open($scope.data.pageDatas[0], 0)
    }
		$('body').scrollTop(0);
        $http({
            method: "GET",
            url: "/pollProj/Templ/evalTempl?page=1"
        }).then(function (res) {
            layer.msg("evalTempl success..");
            $rootScope.data  = res.data;
			console.log($scope.data.totalPages);
        }, function (err) {
            alert("evalTempl err");
        })
		/*$rootScope.data = {
			"code": 0,
			"msg": "",
			"count": 1000,
			data: [{
				"id": 10005,
				"username": "user-5",
				"sex": "女",
				"city": "城市-5",
				"sign": "签名-5",
				"experience": 173,
				"logins": 68,
				"temple_state": 111,
				"templeName": "测试模板6",
				"score": 87,
				state: 1,
				hascon: 1,
				chk: false,
				content: {
					title: "测试模板6",
					code: "ashdk1231",
					beizhu: "beizhu1",
					leimu: [{
						name: "类目1",
						items: ['1.问题1', '2.问题2', '3.问题3', '4.问题4']
					}, {
						name: "类目2",
						items: ['1.问题1', '2.问题2', '3.问题3', '4.问题4']
					}],
					suggestion: ['对老师的建议', '对班级的建议']
				}
			}]
		}-->*/
		layui.use(['table', 'laypage'], function() {
			var table = layui.table;
			var laypage = layui.laypage;
			//执行一个laypage实例
			laypage.render({
				elem: 'pagination', //注意，是 ID，不用加 # 号
				count: 50 //数据总数，从服务端得到
			});

		});
		//ng模态框部分
		//		$rootScope.ddd = "12345qwer"
		// list  
		// open click  
		$scope.open = function(x, disable) {
			var modalInstance = $modal.open({
				templateUrl: 'evalTempl.html',
				controller: 'evalTemplModalCtrl', // specify controller for modal  
				//size: size,
				resolve: {
					items: function() {
						return x;
					}
				}
			});
			// modal return result  
			modalInstance.result.then(function(selectedItem) {
				$scope.selected = selectedItem;
			}, function() {
				$log.info('Modal dismissed at: ' + new Date())
			});
		}
		//模板操作-预览
		$scope.preview = function(x) {

			//弹出编辑模态窗口
			console.log(JSON.stringify(x))
			setCookie('data', JSON.stringify(x), 30)
			window.open("./preview.html")
			//$scope.open(x,1)
		}
		//设置cookie 封装函数
		function setCookie(k, v, ex) {
			var now = new Date()
			var d = now.getDate()
			now.setDate(d + ex)
			document.cookie = k + "=" + v + ";expires=" + now.toUTCString();
		}
		//模板操作-编辑
		$scope.edit = function(x) {
			if(x.state=='0') {
				return layer.confirm("该模板正在使用中，无法编辑", {
					icon: 0,
					title: "提示",
					btn: ['好']
				}, function(index) {
					layer.close(index)
				})
			}
			//弹出编辑模态窗口
			$scope.open(x, 0)
		}
		//模板操作-复制
		$scope.templcopy = function(x) {

            console.log(JSON.stringify(x))
			$http({
				method: "post",
				url: "/pollProj/Templ/copyTemple",
				data:JSON.stringify(x)
			}).then(function(res) {
				layer.msg("模板复制成功")
				$scope.init();
			}, function(err) {
				layer.msg("模板复制失败")
			})
			// $rootScope.data.data.unshift({
			// 	id: x.id + '-copy',
			// 	username: "user-5",
			// 	sex: "女",
			// 	city: "城市-5",
			// 	sign: "签名-5",
			// 	experience: 173,
			// 	logins: 68,
			// 	wealth: '无',
			// 	classify: x.classify + "-副本",
			// 	score: 87,
			// 	state: 0,
			// 	hascon: 1,
			// 	chk: false,
			// 	content: {
			// 		title: x.classify + "-副本",
			// 		code: x.content.code + "-副本",
			// 		beizhu: x.content.beizhu + "-副本",
			// 		leimu: x.content.leimu,
			// 		suggestion: x.content.suggestion
			// 	}
			// })

		}
		//模板操作-删除
		$scope.del = function(x) {
			$http({
				method: "post",
				url: "",
				data: x.id
			}).then(function(res) {
				layer.msg("模板删除成功")
				//$scope.init()
			}, function(err) {
				layer.msg("模板删除失败")
			})
		}
	});
	app.controller('evalTemplModalCtrl', function($rootScope, $http, $scope, $modalInstance, items) {
		$scope.items = items;

		$scope.leimuAdd = function() {
			console.log(items.templeId)
			for(var i = 0; i < $rootScope.data.pageDatas.length; i++) {
				console.log($rootScope.data.pageDatas[i].templeId)
				if($rootScope.data.pageDatas[i].templeId == items.templeId) {
					alert($rootScope.data.pageDatas[i].qmenus[0].qmenu_Name);
					$rootScope.data.pageDatas[i].qmenus.push({
                        qmenuId:1,
                        qmenu_Name: '新类目(待编辑)',
                        questions: [{
                            questionId:1,
                            question_Name:"新问题(待编辑)"
						}]
					})
					console.log($rootScope.data.pageDatas[i])
					//$scope.$apply($rootScope.data)
					break
				}
			}
		}
		$scope.leimuRm = function(index) {
			console.log(items.templeId, index)
			for(var i = 0; i < $rootScope.data.pageDatas.length; i++) {
				console.log($rootScope.data.pageDatas[i].templeId)
				if($rootScope.data.pageDatas[i].templeId == items.templeId) {
					$rootScope.data.pageDatas[i].qmenus.splice(index, 1)
					break
				}
			}
		}
		$scope.leimuItemAdd = function(upindex) {
			console.log(items.templeId)
			for(var i = 0; i < $rootScope.data.pageDatas.length; i++) {
				console.log($rootScope.data.pageDatas[i].templeId)
				if($rootScope.data.pageDatas[i].templeId == items.templeId) {
					$rootScope.data.pageDatas[i].qmenus[upindex].questions.push({
                        questionId:1,
                        question_Name:"新问题(待编辑)"
					})
					break
				}
			}
		}
		$scope.leimuItemRm = function(upindex, index) {
			for(var i = 0; i < $rootScope.data.pageDatas.length; i++) {
				console.log($rootScope.data.pageDatas[i].templeId)
				if($rootScope.data.pageDatas[i].templeId == items.templeId) {
					$rootScope.data.pageDatas[i].qmenus[upindex].questions.splice(index, 1)
					break
				}
			}
		}
		$scope.suggestionAdd = function() {
			console.log(items.templeId)
			for(var i = 0; i < $rootScope.data.pageDatas.length; i++) {
				console.log($rootScope.data.pageDatas[i].templeId)
				if($rootScope.data.pageDatas[i].templeId == items.templeId) {
					$rootScope.data.pageDatas[i].advices.push({
                        adviceId:1,
                        advicename:"新建议(待编辑)"
					})
					break
				}
			}
		}
		$scope.suggestionRm = function(index) {
			console.log(items.templeId, index)
			for(var i = 0; i < $rootScope.data.pageDatas.length; i++) {
				console.log($rootScope.data.pageDatas[i].templeId)
				if($rootScope.data.pageDatas[i].templeId == items.templeId) {
					$rootScope.data.pageDatas[i].advices.splice(index, 1)
					break
				}
			}
		}
		// ok click  
		$scope.ok = function() {
			layer.confirm("确定修改？", {
				icon: 0,
				title: "提示",
				btn: ["确认", "取消"]
			}, function(index) {
				layer.close(index);
			   //发送至后端
				alert(JSON.stringify(items));
				$http({
					method:"post",
					url:"/pollProj/Templ/updateTemple",
					data:JSON.stringify(items)
				}).then(function(res){
                    $modalInstance.close('update ok');
				},function(err){
                    $modalInstance.close('update err');
				})

			}, function(index) {
                $scope.init();
				layer.close(index);
			})
		};
		// cancel click  
		$scope.cancel = function() {
			layer.confirm("确定退出？", {
				icon: 0,
				title: "提示",
				btn: ["确定", "取消"]
			}, function(index) {
				layer.close(index);
				$modalInstance.dismiss('cancel');
			}, function(index) {
				layer.close(index);
			})

		}
	});
});