package com.poll.controller;

import com.poll.entity.PageBean;
import com.poll.entity.Temple;
import com.poll.entity.User;
import com.poll.service.TempleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by XYZ on 2018/1/9.
 */
@Controller
@RequestMapping("/Templ")
public class TempleController {
    @Autowired
    TempleService templeService;

    @ResponseBody
    @RequestMapping(value="/evalTempl" ,produces = "application/json")
    public PageBean<Temple> Evaltempl(@RequestParam(value="pageNow", defaultValue="1")Integer pageNow){
        PageBean<Temple> templePageBean=templeService.getTemplePage(pageNow,10);
        return templePageBean;
    }

    @ResponseBody
    @RequestMapping(value="/getTemplDeatils" ,produces = "application/json")
    public Temple getTemplDeatils(Long templeId){
        return templeService.getTempleInfo(templeId);
    }

    @ResponseBody
    @RequestMapping(value="/copyTemple" ,produces = "application/json")
    public Temple copyTemple(@RequestBody Temple temple){
         templeService.copyTemple(temple);
         return temple;
    }

    @ResponseBody
    @RequestMapping(value="/updateTemple" ,produces = "application/json")
    public Temple updateTemple(@RequestBody Temple temple){
        templeService.updateTemple(temple);
        return temple;
    }
}
