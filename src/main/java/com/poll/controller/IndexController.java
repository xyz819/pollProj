package com.poll.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by XYZ on 2018/1/5.
 */
@Controller
public class IndexController {
    @RequestMapping("/index")
    public String hello(Model model){
        return "WEB-INF/index.jsp";
    };


}
