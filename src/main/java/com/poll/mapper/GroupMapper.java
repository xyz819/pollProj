package com.poll.mapper;
import com.poll.entity.Group;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

/**
 * Created by XYZ on 2017/12/29.
 */
@MapperScan
public interface GroupMapper {
     Group selectgroupById(@Param("id") Long id);

     Integer insterGroup(Group group);

     Integer updateGroup(Group group);

     Integer deleteGroup(Long id);


}
