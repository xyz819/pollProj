package com.poll.mapper;

import com.poll.entity.Advice;
import com.poll.entity.Qmenu;
import com.poll.entity.Question;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * Created by sh on 2017/12/29.
 */
@MapperScan
public interface QmenuMapper {
     /**
      * 增加类目
      */
      Long insertQmenu(@Param("qmenu")Qmenu qmenu, @Param("templeId")Long templeId);

     /**
      * 根据templeid查询
      */
     List<Qmenu> selectQmenusBytempleid(Long templeid);

     /**
      * 根据templeid删除
      */
     Integer deleteQmenu(Long templeid);

     Integer updateQmenu(Qmenu qmenu);
}
