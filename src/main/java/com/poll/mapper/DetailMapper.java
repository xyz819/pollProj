package com.poll.mapper;

import com.poll.entity.Detail;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 */
@MapperScan
public interface DetailMapper {

     /**
      * 通过personalId查询调查明细
      */
     List<Detail> selectByPersonalId(Long personalId);


    /**
      * 添加新的调查明细
      */
     Integer insertDetail(Detail result);


    /**
     * 通过
     * @Parm personalId
     * 删除明细
     */
    Integer delDetailByResultId(Long personalId);

}
