package com.poll.mapper;

import com.poll.entity.Advice;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * Created by sh on 2017/12/29.
 */
@MapperScan
public interface AdviceMapper {
     //根据id查询建议
     List<Advice> selectByTempleId(Long id);

     void deleteAdviceById(Long id);

     Integer insertAdvice(@Param("advice")Advice advice, @Param("templeId")Long templeId);

     Integer updateAdvice(Advice advice);


}
