package com.poll.mapper;

import com.poll.entity.Clazz;
import com.poll.entity.ClazzType;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

@MapperScan
public interface ClazzMapper {

     Clazz selectById(Long id);

     Integer insertClazz(Clazz clazz);

     Integer deleteClazzById(Long id);

     Integer updateClazz(Clazz clazz);

     List<Clazz> selectAllClazz();

}
