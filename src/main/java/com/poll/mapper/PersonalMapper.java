package com.poll.mapper;

import com.poll.entity.Personal;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * Created by XYZ on 2018/1/4.
 */
@MapperScan
public interface PersonalMapper {
    /**
     * 通过PersonalId查询单次结果
     */
    Personal selectById(Long PersonalId);

    /**
     * 使用page查询单次结果
     */
    List<Personal> selectPersonalList(@Param("pageNow") Integer pageNow, @Param("pageSize") Integer pageSize, @Param("resultId")Long resultId);

    /**
     * 添加新的单次结果
     */
    Integer insertPersonal(Personal result);
    /**
     * 修改单次结果有效性
     * （有效（1），无效（0））
     */
    Integer updateState(Personal result);

    /**
     * 通过
     * @Parm resultId
     * 删除单次结果
     */
    Integer delPersonalByResultId(Long resultId);}
