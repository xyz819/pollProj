package com.poll.mapper;
import com.poll.entity.Group;
import com.poll.entity.User;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 */
@MapperScan
public interface UserMapper {
     User selectuserByusername(@Param("username") String username);

     Integer insterUser(User user);

     Integer updateUser(User user);

     Integer deleteUser(Long id);

     List<User> selectUserBygroupid(long groupid);

     List<User> selectUserbypage(@Param("pageNow") int pageNow, @Param("pageSize") int pageSize);

     User selectUser(@Param("username") String username, @Param("password") String password);


}
