package com.poll.mapper;

import com.poll.entity.Advice;
import com.poll.entity.Question;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * Created by sh on 2017/12/29.
 */
@MapperScan
public interface QuestionMapper {
     /**
      * 增加问题
      * */
     Integer insertQuestion(@Param("question")Question question,@Param("qmenuId")Long qmenuId);

     /**
      * 根据menuid删除问题
      */
     Integer deleteQuestionById(Long qmenuId);

     /**
     根据menuid查询
      */
     List<Question> selectQuestionsByMenuId(Long qmenuId);

     Integer updateQuestion(Question question);
}
