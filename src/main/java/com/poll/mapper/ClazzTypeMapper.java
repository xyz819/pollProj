package com.poll.mapper;

import com.poll.entity.ClazzType;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 */
@MapperScan
public interface ClazzTypeMapper {

     ClazzType selectById(Long id);

     List<ClazzType> selectAllClazzType();

}

