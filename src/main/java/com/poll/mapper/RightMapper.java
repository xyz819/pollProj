package com.poll.mapper;
import com.poll.entity.Right;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 */
@MapperScan
public interface RightMapper {
     Right selectById(@Param("id") Long id);

     Integer insterRight(Right right);

     Integer updateRight(Right right);

     Integer deleteRightbyId(Long id);
}
