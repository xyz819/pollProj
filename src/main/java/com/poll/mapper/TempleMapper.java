package com.poll.mapper;

import com.poll.entity.Temple;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TempleMapper {

    /**
     * 增加
     */
    Long insertTemple(Temple temple);

     /**
      * 查询
      */
    Temple selectTempleById(Long templeId);

     /**
      * 删除
      */
     Integer deleteTemple(Long id);

    /**
     * 分页查询
     */
    List<Temple> selectTempleByPage(@Param("pageNow")Integer pageNow,@Param("pageSize")Integer pageSize);
    /**
    *查询总数量
     */
    Integer countTemple();
    /**
     * 更新模板
     */
    Integer updateTemple(Temple temple);

}
