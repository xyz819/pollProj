package com.poll.mapper;

import com.poll.entity.PResult;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.Date;
import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 */
@MapperScan
public interface PResultMapper {

     /**
      * 通过ResultID查询调查结果
      */
     PResult selectById(Long id);

     /**
      * 使用page查询调查结果
      */
     List<PResult> selectResultList(@Param("pageNow") Integer pageNow, @Param("pageSize") Integer pageSize);

     /**
      * 添加新的调查结果
      */
     Integer insertResult(PResult result);

     /**
      * 更新调查结果状态
      * （已审核（1），未审核（0））
      */
     Integer updateState(PResult  result);

     /**
      * 删除PResult
      */
     Integer delResultById(Long id);

     /**
      * 查询两时间之间的Result
      */
     List<PResult> selectByTime(@Param("starttime")Date starttime,@Param("endtime")Date endtime);

     Integer selectResultCount();

}
