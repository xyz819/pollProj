package com.poll.service.impl;


import com.poll.entity.*;
import com.poll.mapper.AdviceMapper;
import com.poll.mapper.QmenuMapper;
import com.poll.mapper.QuestionMapper;
import com.poll.mapper.TempleMapper;
import com.poll.service.TempleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author Administrator
 */
@Service
@Transactional
public class TempleServiceImpl implements TempleService{
    @Autowired
    TempleMapper templeMapper;
    @Autowired
    QmenuMapper qmenuMapper;
    @Autowired
    AdviceMapper adviceMapper;
    @Autowired
    QuestionMapper questionMapper;
    @Override
    public PageBean<Temple> getTemplePage(Integer pageNow, Integer pageSize) {
        PageBean<Temple> templePageBean = new PageBean<>();
        templePageBean.setPageNow(pageNow);
        templePageBean.setPageSize(pageSize);
        templePageBean.setTotalRecords(templeMapper.countTemple());
        templePageBean.setPageDatas(templeMapper.selectTempleByPage(pageNow,pageSize));
        return templePageBean;
    }

    @Override
    public Integer saveTemple(Temple temple) {
        templeMapper.insertTemple(temple);
        //级联插入类目menu
        for (Advice advice:temple.getAdvices()) {
            adviceMapper.insertAdvice(advice,temple.getTempleId());
        }
        for (Qmenu qmenu:temple.getQmenus()) {
            qmenuMapper.insertQmenu(qmenu,temple.getTempleId());
            for (Question question:qmenu.getQuestions()) {
                questionMapper.insertQuestion(question,qmenu.getQmenuId());
            }
        }
        return 1;
    }

    @Override
    public Temple getTempleInfo(Long templeId) {
        return templeMapper.selectTempleById(templeId);
    }

    @Override
    public Integer updateTemple(Temple temple) {

        for (Advice advice:temple.getAdvices()) {
            adviceMapper.updateAdvice(advice);
        }
        for (Qmenu qmenu:temple.getQmenus()) {
            for (Question question:qmenu.getQuestions()) {
                questionMapper.updateQuestion(question);
            }
            qmenuMapper.updateQmenu(qmenu);
        }
        templeMapper.updateTemple(temple);
        return null;
    }

    @Override
    public void copyTemple(Temple temple) {
        temple.setTempleName(temple.getTempleName()+"-副本");
        temple.setDescription(temple.getDescription()+"-副本");
        saveTemple(temple);
    }
}
