package com.poll.service.impl;

import com.poll.entity.User;
import com.poll.mapper.GroupMapper;
import com.poll.mapper.UserMapper;
import com.poll.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.List;

/**
 * Created by XYZ on 2018/1/3.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;
    GroupMapper groupMapper;


    @Override
    public Integer ckRegister(String username, String phone_num, String email) {
            Integer i = null;
                if(username!=null) {
                    i += 2;
                }if(phone_num!=null){
                    i+=2;
                }
                if(email!=null){
                  i+=2;
                }

        return i;
    }

    @Override
    public Integer register(User user) {

        return userMapper.insterUser(user);
    }

    @Override
    public User login(String username, String password) {

        User user=userMapper.selectUser(username, password);

        return user;
    }


    @Override
    public List<User> getVictimName(Long groupId) {

          List<User> user= userMapper.selectUserBygroupid(groupId);

        return user;
    }
}
