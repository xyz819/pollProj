package com.poll.service.impl;

import com.poll.entity.Clazz;
import com.poll.mapper.ClazzMapper;
import com.poll.service.ClazzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by 峰哥 on 2018/1/3.
 */
@Service
@Transactional
public class ClazzServiceImpl implements ClazzService{

    @Autowired
    ClazzMapper clazzMapper;

    @Override
    public Integer addClazz(Clazz clazz) {
        return clazzMapper.insertClazz(clazz);
    }

    @Override
    public Integer updateClazz(Clazz clazz) {
        return clazzMapper.updateClazz(clazz);
    }

    @Override
    public List<Clazz> getAllClazz() {
        return clazzMapper.selectAllClazz();
    }
}
