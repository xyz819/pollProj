package com.poll.service.impl;

import com.poll.entity.PResult;
import com.poll.mapper.PResultMapper;
import com.poll.service.PResultService;
import com.poll.entity.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by XYZ on 2018/1/3.
 */
@Service
@Transactional
public class PResultServiceImpl implements PResultService {

    @Autowired
    PResultMapper pResultMapper;

    @Override
    public Integer addPResult(PResult pResult) {
        return pResultMapper.insertResult(pResult);
    }

    @Override
    public Integer delPResult(Long pResultId) {
        return pResultMapper.delResultById(pResultId);
    }

    @Override
    public Integer changePResultState(PResult pResult) {
        return pResultMapper.updateState(pResult);
    }

    @Override
    public PageBean<PResult> findPResultPage(Integer pageNow, Integer pageSize) {
        PageBean<PResult> resultPageBean=new PageBean<>();
        resultPageBean.setPageDatas(pResultMapper.selectResultList(pageNow,pageSize));
        resultPageBean.setTotalRecords(pResultMapper.selectResultCount());
        return resultPageBean;
    }

    @Override
    public List<PResult> findResultListByYear(Date cktime) {
        Integer year=cktime.getYear();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟
        String d1=year+"-01-01 00:00:00";
        String d2=year+"-12-31 23:59:59";
        try {
            Date starttime=sdf.parse(d1);
            Date endtime=sdf.parse(d2);
            return pResultMapper.selectByTime(starttime,endtime);
        } catch (ParseException e) {
            System.out.println("年查询转换格式错误");
            return null;
        }
    }

    @Override
    public List<PResult> findResultListByMonth(Date cktime) {
        Integer year=cktime.getYear();
        Integer month=cktime.getMonth();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟
        String d1=year+"-"+month+"-01 00:00:00";
        String d2=year+"-"+month+"-31 23:59:59";
        try {
            Date endtime=sdf.parse(d2);
            Date starttime=sdf.parse(d1);
            return pResultMapper.selectByTime(starttime,endtime);
        } catch (ParseException e) {
            System.out.println("月查询转换格式错误");
            return null;
        }
    }

    @Override
    public List<PResult> findResultListBySeason(Date cktime) {
        Integer year=cktime.getYear();
        Integer month=cktime.getMonth();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟
        String d1=year+"-"+month+"-01 00:00:00";
        String d2=year+"-"+(month+3)+"-31 23:59:59";
        try {
            Date endtime=sdf.parse(d2);
            Date starttime=sdf.parse(d1);
            return pResultMapper.selectByTime(starttime,endtime);
        } catch (ParseException e) {
            System.out.println("季度查询转换格式错误");
            return null;
        }
    }
}
