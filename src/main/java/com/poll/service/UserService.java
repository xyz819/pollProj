package com.poll.service;

import com.poll.entity.User;

import java.util.List;

/**
 * Created by XYZ on 2018/1/2.
 */
public interface UserService {

    /**
     * 验证注册
     * @Parm username,
     * @Parm phone_num,
     * @Parm email
     * 是否被注册
     * 对应3位二进制数 111B,表示都被注册
     */

    Integer ckRegister(String username, String phone_num, String email);

    /**
     * 注册实现
     */
    Integer register(User user);

    /**
     * 登录:通过
     * @Parm username
     * 查询出User实体类
     */
    User login(String username, String password);
    /**
     * 获得被调查人列表
     * @Parm 被调查人的GroupId
     */
    List<User>   getVictimName(Long groupId);
}
