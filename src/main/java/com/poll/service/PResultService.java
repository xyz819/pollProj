package com.poll.service;

import com.poll.entity.PResult;
import com.poll.entity.PageBean;

import java.util.Date;
import java.util.List;

/**
 * Created by XYZ on 2018/1/2.
 */
public interface PResultService {
    /**
     * 新建结果并保存
     */
    Integer addPResult(PResult pResult);

    /**
     * 删除结果
     */
    Integer delPResult(Long pResultId);

    /**
     * 修改结果状态
     */
    Integer changePResultState(PResult pResult);

    /**
     * 获得pageBean<PResult>
     */
    PageBean<PResult> findPResultPage(Integer pageNow, Integer pageSize);

    /**
     * 按年返回List
     */
    List<PResult> findResultListByYear(Date cktime);


    /**
     * 按月返回List
     */
    List<PResult> findResultListByMonth(Date cktime);

    /**
     * 按季返回List
     */
    List<PResult> findResultListBySeason(Date cktime);



}
