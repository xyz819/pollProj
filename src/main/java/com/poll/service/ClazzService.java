package com.poll.service;

import com.poll.entity.Clazz;
import com.poll.entity.ClazzType;

import java.util.List;

/**
 * Created by XYZ on 2018/1/2.
 */
public interface ClazzService {
    /**
     * 新增班级
     * @Parm Clazz
     */
    Integer addClazz(Clazz clazz);

    /**
     * 修改班级
     *@Parm Clazz
     */
    Integer updateClazz(Clazz clazz);

    /**
     * 获取班级列表
     */
    List<Clazz> getAllClazz();
}
