package com.poll.service;

import com.poll.entity.Temple;
import com.poll.entity.PageBean;

/**
 * Created by XYZ on 2018/1/3.
 */
public interface TempleService {
    /**
     * 通过
     * @Parm pageNow
     * @Paem pageSize
     * >>获取模版列表(只查询temple表 不需要级联查询)<<
     * >>用于模版管理<<
     */
    PageBean<Temple> getTemplePage(Integer pageNow , Integer pageSize);

    /**
     * 插入一个Temple
     * >>同一个事务中 将 temple,advice,Qmenu,Question分别保存
     * >>在内存中新建temole 添加内容最后再保存进数据库<<
     */
    Integer saveTemple(Temple temple);

    /**
     * 通过
     * @Parm templeId
     * 获取一个Temple的详细内容
     * >>需要级联查询<<
     * >>用于修改temple和新建调查<<
     */
    Temple getTempleInfo(Long templeId);

    /**
     * 通过
     * @Parm Temple
     * 更新Temple
     */
    Integer updateTemple(Temple temple);

    /**
     * 拷贝
     * @param temple
     */
    void copyTemple(Temple temple);
}
