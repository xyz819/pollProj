package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 * @Class 调查结果
 */
@Data
@NoArgsConstructor
public class PResult implements Serializable {
    private Long resultId;
    //结果编号
    private String result_sid;
    //班级名
    private String clazzName;
    //班级类型
    private String clazzTypeName;
    //被调查人名字
    private String victimName;
    //开始调查时间
    private Date startTime;
    //结束调查时间
    private Date endTime;
    //参与调查人数量
    private Long userCount;
    //有效人数
    private Long validity;
    //结果状态（已审核（1），未审核（0））
    private Integer result_state;
    //平均星值（最高五星）
    private String staravg;
    //单次提交列表
    private List<Personal> personals;
}
