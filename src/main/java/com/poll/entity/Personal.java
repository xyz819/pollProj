package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by XYZ on 2018/1/4.
 */
@Data
@NoArgsConstructor
public class Personal implements Serializable {
    private Long personalId;
    //每个单次提交对应的调查结果
    private Long resultId;
    //单词结果平均分
    private String staravg;
    //是否有效（有效（1），无效（0））
    private Integer personal_state;
    //提交时间
    private Date sendTime;
    //单次提交对应的多个明细
    List<Detail> details;
}
