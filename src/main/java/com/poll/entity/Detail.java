package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by XYZ on 2017/12/29.
 * @Class 调查结果明细
 */
@Data
@NoArgsConstructor
public class Detail implements Serializable {
    private Long detailId;
    //明细对应的单次提交
    private Long personalId;
    //明细的Key和Value
    private String q_key;
    private String a_value;
    private Integer key_type;
}
