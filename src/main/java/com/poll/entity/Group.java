package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by admin on 2017/12/29.
 * @Class 权限组
 */
@Data
@NoArgsConstructor
public class Group {
    private Long groupid;
    //权限组名
    private String groupname;
    //权限列表
    private List<Right> rights;
}
