package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by admin on 2017/12/29.
 * @Class 用户
 */
@Data
@NoArgsConstructor
public class User {
    private Long   userid;
    //用户名
    private String username;
    //用户密码
    private String password;
    //电话号码
    private String phone_num;
    //邮箱
    private String email;
    //创建时间
    private Date   init_time;
    //最后登陆时间
    private Date   last_login_time;
    //登录次数
    private Long   login_count;
    //所属权限组
    private Group  group;

}
