package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by sh on 2018/1/2.
 * @Class 类目
 */
@Data
@NoArgsConstructor
public class Qmenu {
    private Long qmenuId;
    //类目名
    private String qmenu_Name;
    //类目下问题列表
    private List<Question> questions;
}
