package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author 峰哥
 * @date 2017/12/29
 * @Class 班级
 */
@Data
@NoArgsConstructor
public class Clazz implements Serializable{
    private Long clazzid;
    //班级名
    private String clazzname;
    //班级创建时间
    private Date clazz_create_date;
    //备注
    private String description;
    //班级状态 毕业（1）和未毕业（0）
    private int clazz_state;
    //班级类型
    private  ClazzType clazztype;
}
