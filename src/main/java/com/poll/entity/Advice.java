package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by sh on 2018/1/2.
 * @Class 意见
 */
@Data
@NoArgsConstructor
public class Advice implements Serializable{
    private Long adviceId;
    //给出的建议
    private String advicename;
}
