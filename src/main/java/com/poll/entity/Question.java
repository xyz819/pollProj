package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sh on 2018/1/2.
 * @Class 问题
 */
@Data
@NoArgsConstructor
public class Question {
    private Long questionId;
    //问题名
    private String question_Name;
}
