package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by sh on 2018/1/2.
 *  @Class 模版
 */
@Data
@NoArgsConstructor
public class Temple {
    private Long templeId;
    //模版名
    private String templeName;
    //模版编号
    private String temple_sid;
    //模版状态 (1.可用 ;0.禁用)
    private String temple_state;
    //备注
    private String description;
    //意见列表
    private List<Advice> advices;
    //类目列表
    private List<Qmenu> qmenus;
}
