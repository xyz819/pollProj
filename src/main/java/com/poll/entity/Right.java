package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by admin on 2017/12/29.
 * @Class 权限
 */
@Data
@NoArgsConstructor
public class Right {
    private Long rightid;
    //权限名
    private String rightname;
    //权限对应URL
    private String righturl;
    //父权限
    private String  parent_right;
    //备注
    private String description;
}
