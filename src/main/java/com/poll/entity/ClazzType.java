package com.poll.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by XYZ on 2017/12/29.
 * @Class 班级类型
 */
@Data
@NoArgsConstructor
public class ClazzType implements Serializable {
    private Long clazzTypeId;
    //班级类型名
    private String clazzTypeName;
    //备注
    private String description;
}
