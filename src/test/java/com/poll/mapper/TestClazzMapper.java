package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.Clazz;
import com.poll.entity.ClazzType;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 */
public class TestClazzMapper extends BasicTest{
    @Autowired
    ClazzMapper clazzMapper;

    @Test
    public void testSelectById() {
        Clazz clazz = clazzMapper.selectById(1L);
        System.out.println(clazz);
    }
    @Test
    public void testInsertClazz(){
        Clazz clazz = new Clazz();
        ClazzType clazztype = new ClazzType();
        clazztype.setClazzTypeId(1L);
        clazz.setClazztype(clazztype);
        clazz.setClazzname("呵呵");
        clazz.setDescription("战斗");
        clazz.setClazz_create_date(new Date());
        clazz.setClazz_state(0);
        clazzMapper.insertClazz(clazz);
    }
    @Test
    public void testDeleteById(){
        clazzMapper.deleteClazzById(2L);
    }
    @Test
    public void testUpdateClazz(){
        Clazz clazz = new Clazz();
        clazz.setClazzid(1L);
        clazz.setClazzname("库里");
        clazz.setDescription("战斗");
        clazz.setClazz_create_date(new Date());
        clazz.setClazz_state(0);
        clazzMapper.updateClazz(clazz);
    }
    @Test
    public  void testSelectAllClazz(){
        List l =clazzMapper.selectAllClazz();
        System.out.println(l.size());
    }
}
