package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.Question;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by sh on 2018/1/3.
 */
public class TestQuestionMapper extends BasicTest{
    @Autowired
    QuestionMapper questionMapper;

    @Test
    public void testinsertQuestion(){
        Question question = new Question();
        question.setQuestion_Name("iii");
        System.out.println(questionMapper.insertQuestion(question,3L));
    }
    @Test
    public void testDeleteQuestion(){
        questionMapper.deleteQuestionById(1L);

    }

    @Test
    public void testSelectQuestion(){
        System.out.println(questionMapper.selectQuestionsByMenuId(2L).size());



    }

}
