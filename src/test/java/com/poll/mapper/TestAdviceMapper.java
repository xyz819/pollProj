package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.Advice;
import com.poll.entity.ClazzType;
import com.poll.entity.Temple;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by sh on 2017/12/29.
 */
public class TestAdviceMapper extends BasicTest{
    @Autowired
    AdviceMapper adviceMapper;

    @Test
    public void testDeleteById() {
        adviceMapper.deleteAdviceById(1L);
    }

    @Test
    public void testSelectByTempleId() {
        System.out.println(  adviceMapper.selectByTempleId(1L).size());

    }

    @Test
    public void testinsertAdvice() {
        Advice advice = new Advice();
        advice.setAdviceId(5L);
        advice.setAdvicename("uuu");
        adviceMapper.insertAdvice(advice,102l);
    }




}

