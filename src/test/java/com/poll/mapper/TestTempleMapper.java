package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.Clazz;
import com.poll.entity.ClazzType;
import com.poll.entity.Temple;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 */
public class TestTempleMapper extends BasicTest{
    @Autowired
    TempleMapper templeMapper;

    @Test
    public void testSelectById() {
        System.out.println(templeMapper.selectTempleById(102L));
    }

    @Test
    public void testinsertTemple() {
        Temple temple = new Temple();
        temple.setTempleName("sds");
        temple.setTemple_state("1");
        temple.setDescription("kkk");
        temple.setTemple_sid("aaa");
        System.out.println( templeMapper.insertTemple(temple));
    }

    @Test
    public  void  testdeleteTemple(){
        templeMapper.deleteTemple(107L);
    }

    @Test
    public  void  testselectTempleByPage(){
        System.out.println(templeMapper.selectTempleByPage(1,3).size());
    }
    @Test
    public  void  testcountTemple(){
        System.out.print(templeMapper.countTemple());
    }
    @Test
    public void testUpdateTemple(){
        Temple temple = new Temple();
        temple.setTempleId(5L);
        temple.setDescription("jj");
        temple.setTemple_sid("dd");
        temple.setTempleName("jh");
        temple.setTemple_state("ui");
        templeMapper.updateTemple(temple);
    }

}
