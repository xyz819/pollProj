package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.ClazzType;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 */
public class TestClazzTypeMapper extends BasicTest{
    @Autowired
    ClazzTypeMapper clazzTypeMapper;

    @Test
    public void testSelectById() {
        ClazzType clazztp = clazzTypeMapper.selectById(1L);
        System.out.println(clazztp);
    }
    @Test
    public  void testSelectAllClazzType(){
        List l =clazzTypeMapper.selectAllClazzType();
        System.out.println(l.size());
    }
}
