package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.Detail;
import com.sun.org.apache.xpath.internal.SourceTree;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;


/**
 * Created by XYZ on 2017/12/29.
 */
public class TestDetailMapper extends BasicTest{

    @Autowired
    DetailMapper detailMapper;
    @Test
    public void selectByPersonalId() throws Exception {
        detailMapper.selectByPersonalId(1L).forEach(System.out::println);
    }


    @Test
    public void insertDetail() throws Exception {
        Detail detail= new Detail();
        detail.setPersonalId(1L);
        detail.setQ_key("对老师的评价:该老师讲课风趣么");
        detail.setA_value("4");
        detail.setKey_type(1);
        detailMapper.insertDetail(detail);
    }


}