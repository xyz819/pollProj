package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.Qmenu;
import com.poll.entity.Question;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by sh on 2018/1/3.
 */
public class TestQmenuMapper extends BasicTest{
    @Autowired
    QmenuMapper qmenuMapper;

    @Test
    public void testinsertQmenu(){
        Qmenu qmenu = new Qmenu();
        qmenu.setQmenu_Name("dlf");
        qmenuMapper.insertQmenu(qmenu,103L);
    }

    @Test
    public void testSelectQmenu(){
        System.out.println(qmenuMapper.selectQmenusBytempleid(102L).size());

    }

    @Test
    public void testDeleteQmenu(){
       qmenuMapper.deleteQmenu(2L);

    }
}
