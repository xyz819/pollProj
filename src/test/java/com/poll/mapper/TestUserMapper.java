package com.poll.mapper;

import com.poll.BasicTest;


import com.poll.entity.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by XYZ on 2017/12/29.
 */
public class TestUserMapper extends BasicTest{
    @Autowired
    UserMapper userMapper;

    @Test
    public void testselectuserByusername() {
        User user = userMapper.selectuserByusername("aaa");
        System.out.println(user);
    }
    @Test
    public void testselectUserBygroupid() {
        List<User> list = userMapper.selectUserBygroupid(1l);
        System.out.println(list);
    }

    @Test
    public void testSelectUser() {
        User user = userMapper.selectUser("admin", "123456");
        System.out.println(user);
    }

}
