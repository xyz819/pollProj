package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.Personal;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by XYZ on 2018/1/4.
 */
public class PersonalMapperTest extends BasicTest {
    @Autowired
    PersonalMapper personalMapper;
    @Test
    public void selectById() throws Exception {
        System.out.println(personalMapper.selectById(1L));
    }

    @Test
    public void selectPersonalList() throws Exception {
        personalMapper.selectPersonalList(1,5,1l).forEach(System.out::println);
    }

    @Test
    public void insertPersonal() throws Exception {
        Personal personal=new Personal();
        personal.setResultId(1l);
        personal.setStaravg("4.3");
        personal.setPersonal_state(1);
        personal.setSendTime(new Date());
        personalMapper.insertPersonal(personal);
    }

    @Test
    public void updateState() throws Exception {
        Personal personal=new Personal();
        personal.setPersonalId(1l);
        personal.setPersonal_state(0);
        personalMapper.updateState(personal);
    }

}