package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.Right;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by XYZ on 2017/12/29.
 */
public class TestRightMapper extends BasicTest{
    @Autowired
    RightMapper rightMapper;

    @Test
    public void testSelectById() {
        Right right = rightMapper.selectById(1l);
        System.out.println(right);
    }

    @Test
    public void testdeleteRightbyId() {
     Integer right= rightMapper.deleteRightbyId(1l);
        System.out.println(right);
    }

    @Test
    public void testinsterRight() {
        Right right = new Right();
            right.setRightid(1l);
            right.setRightname("as");
            right.setRighturl("ssss");
            right.setParent_right("aassdqq");
            right.setDescription("这是描述");
            rightMapper.insterRight(right);
    }

}

