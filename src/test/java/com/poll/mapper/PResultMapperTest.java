package com.poll.mapper;

import com.poll.BasicTest;
import com.poll.entity.PResult;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by XYZ on 2018/1/2.
 */
public class PResultMapperTest extends BasicTest {

    @Autowired
    PResultMapper pResultMapper;
    @Test
    public void selectById() throws Exception {
        System.out.println(pResultMapper.selectById(1l));
    }

    @Test
    public void selectResultList() throws Exception {
        List<PResult> details = pResultMapper.selectResultList(1,2);
        System.out.println(details.size());
    }

    @Test
    public void insertResult() throws Exception {
        PResult pResult=new PResult();
        pResult.setClazzName("土木二班");
        pResult.setClazzTypeName("土木工程");
        pResult.setEndTime(new Date());
        pResult.setResult_sid("tm-2018");
        pResult.setResult_state(1);
        pResult.setStaravg("4.8");
        pResult.setStartTime(new Date());
        pResult.setUserCount(123L);
        pResult.setValidity(121L);
        pResult.setVictimName("李刚");
        pResultMapper.insertResult(pResult);
    }

    @Test
    public void updateState(){
        PResult pResult=new PResult();
        pResult.setResult_state(0);
        pResult.setResultId(1L);
        pResultMapper.updateState(pResult);
    }

    @Test
    public void selectByTime(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟
        String d1="2018-1-1 12:12:12";
        String d2="2018-4-24 12:12:12";
        try {
            Date starttime=sdf.parse(d1);
            Date endtime=sdf.parse(d2);
            pResultMapper.selectByTime(starttime,endtime).forEach(System.out::println);
        } catch (ParseException e) {
            System.out.println("转换格式错误");
            e.printStackTrace();
        }

    }


}