package com.poll.service.impl;

import com.poll.BasicTest;
import com.poll.entity.Clazz;
import com.poll.entity.ClazzType;
import com.poll.mapper.ClazzMapper;
import com.poll.service.ClazzService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by 峰哥 on 2018/1/3.
 */
public class ClazzServiceImplTest extends BasicTest{
    @Autowired
    ClazzService clazzService;
    @Test
    public void addClazz() throws Exception {
        Clazz clazz = new Clazz();
        ClazzType clazzType = new ClazzType();
        clazz.setClazzid(3L);
        clazz.setClazz_create_date(new Date());
        clazz.setClazzname("格林");
        clazz.setClazz_state(0);
        clazz.setDescription("哈哈一笑");
        clazzType.setClazzTypeId(1L);
        clazz.setClazztype(clazzType);
        clazzService.addClazz(clazz);
    }

    @Test
    public void updateClazz() throws Exception {

    }

    @Test
    public void getAllClazz() throws Exception {
        System.out.println(clazzService.getAllClazz());

    }

}