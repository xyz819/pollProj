package com.poll.service.impl;

import com.poll.BasicTest;
import com.poll.entity.*;
import com.poll.service.TempleService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 峰哥 on 2018/1/3.
 */
public class TempleServiceImplTest extends BasicTest{
    @Autowired
    TempleService templeService;
    @Test
    public void testSelectById(){

        System.out.println( templeService.getTempleInfo(102L));
    }
    @Test
    public  void testGetTemplePage(){
        System.out.println(templeService.getTemplePage(1, 2));
    }
    @Test
    public void testSaveTemple(){
        Temple temple = new Temple();
        temple.setTempleName("hh");
        temple.setTemple_sid("ee");
        temple.setTemple_state("1");
        temple.setTemple_state("yu");
        temple.setDescription("分隔");
        Advice advice = new Advice();
        advice.setAdvicename("国家");
        List<Advice> advices = new ArrayList<>();
        advices.add(advice);
        temple.setAdvices(advices);
        List<Qmenu> qmenus = new ArrayList<>();
        List<Question> questions = new ArrayList<>();
        Qmenu qmenu = new Qmenu();
        Question question = new Question();
        question.setQuestion_Name("hh");
        qmenu.setQmenu_Name("jj");
        qmenus.add(qmenu);
        questions.add(question);
        qmenu.setQuestions(questions);
        temple.setQmenus(qmenus);
        templeService.saveTemple(temple);
    }
    @Test
    public void testUpdateTemple(){
//        Temple temple = new Temple();
        Temple temple=templeService.getTempleInfo(1l);
        temple.setTempleName("hha");
        temple.setTemple_sid("eea");
        temple.setTemple_state("1a");
        temple.setTemple_state("1");
        temple.setDescription("currya");
        for (Qmenu menu:temple.getQmenus()
             ) {
            menu.setQmenu_Name(menu.getQmenu_Name()+'a');
            for (Question q:menu.getQuestions()
                 ) {
                q.setQuestion_Name(q.getQuestion_Name()+'a');
            }
        }
        templeService.updateTemple(temple);
    }

}